/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primeirasequencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Aluno
 */
public class Interesse {
    private int id_interesse;
    private Usuario usuario;
    private String nome_interesse;

    public int getId_interesse() {
        return id_interesse;
    }

    public void setId_interesse(int id_interesse) {
        this.id_interesse = id_interesse;
    }

    public Usuario getId_usuario() {
        return usuario;
    }

    public void setId_usuario(Usuario id_usuario) {
        this.usuario = id_usuario;
    }

    public String getNome_interesse() {
        return nome_interesse;
    }

    public void setNome_interesse(String nome_interesse) {
        this.nome_interesse = nome_interesse;
    }


public int inserirInteresse(Interesse interesse) {

        Conexao c = new Conexao();

        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Statement st = null;
        int codigoVenda = 0;

        String insertTableSQL = "INSERT INTO OO_Interesse"
                + "(id_interesse, id_usuario, nome_interesse) VALUES"
                + "(seq_interessa.nextval,?,?)";

        try {

            String generatedColumns[] = {"id_interesse"};

            ps = dbConnection.prepareStatement(insertTableSQL, generatedColumns);

            st = dbConnection.createStatement();
               
            ps.setInt(1, usuario.getId_usuario());
            ps.setString(2, interesse.getNome_interesse());

            //execute insert SQL statement
            ps.executeUpdate();

            //ver qual codigo da tua venda
            System.out.println("Record is inserted into OO_UsuarioSeq table!");

            ResultSet rs = ps.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
                System.out.println("id gerado: " + chaveGerada);
            }
            interesse.setId_interesse(chaveGerada);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id_interesse;
    }
}
