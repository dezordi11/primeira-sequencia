/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primeirasequencia;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author Aluno
 */
public class FXMLDocumentController implements Initializable {
    
    private Label label;
    @FXML
    private TextField txReSenha;
    @FXML
    private TextField txInteresse;
    @FXML
    private TextField txSenha;
    @FXML
    private TextField txNomeUsuario;
    private int i;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void cadastrar(ActionEvent event) {
                Usuario ml = new Usuario();
       
            ml.setNome(txNomeUsuario.getText());
            ml.setSenha(Integer.parseInt(txSenha.getText()));
            ml.inserirUsuario(ml);
            
            String[] interesses = txInteresse.getText().split(",");
            for(i= 0; i<interesses.length; i++){
                Interesse in = new Interesse();
            in.setNome_interesse(interesses[i]);
            in.setId_usuario(ml);
            in.inserirInteresse(in);
            }
    }
    
}
