/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primeirasequencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Aluno
 */
public class Usuario {
    private int id_usuario;
    private String nome;
    private int senha;

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getSenha() {
        return senha;
    }

    public void setSenha(int senha) {
        this.senha = senha;
    }
     public int inserirUsuario(Usuario usuario) {

        Conexao c = new Conexao();

        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Statement st = null;
        int codigoVenda = 0;

        String insertTableSQL = "INSERT INTO OO_UsuarioSeq"
                + "(id_usuario, nome, senha) VALUES"
                + "(seq_usuario.nextval,?,?)";

        try {

            String generatedColumns[] = {"id_usuario"};

            ps = dbConnection.prepareStatement(insertTableSQL, generatedColumns);

            st = dbConnection.createStatement();
               
            ps.setString(1, usuario.getNome());
            ps.setInt(2, usuario.getSenha());

            //execute insert SQL statement
            ps.executeUpdate();

            //ver qual codigo da tua venda
            System.out.println("Record is inserted into OO_UsuarioSeq table!");

            ResultSet rs = ps.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
                System.out.println("id gerado: " + chaveGerada);
            }
            usuario.setId_usuario(chaveGerada);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id_usuario;
    }
}
